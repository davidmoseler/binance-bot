import time
import os
import hmac
import hashlib
import urllib
import requests
import time

def current_time():
    return round(time.time() * 1000)

public_key='ytGs1cfDtC27uMNiefnfb1p1eFhtPXBtjsQK1fojrBUvhLMuCvMB9F3Ypa8BjLlW'
private_key='KEjSwR9iCdR5jB7inea2t0KMuu2AhGs5qvzhu7HOinT3CWu14y4fJRsCIK2B3Jak'

pairs = ['BNBBTC', 'TRXBTC', 'XRPBTC', 'ETHBTC', 'BNBUSDT', 'BTCUSDT', 'ETHUSDT', 'TRXUSDT', 'XRPUSDT', 'EOSBTC', 'EOSUSDT', 'LINKBTC', 'LINKUSDT', 'ONTBTC', 'ONTUSDT', 'ADABTC', 'ADAUSDT', 'BTCUSDC', 'ETCUSDT', 'ETCBTC', 'LTCBTC', 'LTCUSDT', 'XLMBTC', 'XLMUSDT', 'USDCUSDT', 'XMRBTC', 'XMRUSDT', 'NEOBTC', 'NEOUSDT', 'ATOMBTC', 'ATOMUSDT', 'DASHBTC', 'DASHUSDT', 'ZECUSDT', 'ZECBTC', 'MATICBTC', 'MATICUSDT', 'BTCBUSD', 'BATBTC', 'BATUSDT', 'IOSTBTC', 'IOSTUSDT', 'VETBTC', 'VETUSDT', 'QTUMUSDT', 'QTUMBTC', 'XRPETH', 'BNBETH', 'IOTABTC', 'IOTAUSDT', 'XTZBTC', 'XTZUSDT', 'BCHBTC', 'BCHUSDT', 'RVNBTC', 'RVNUSDT', 'BUSDUSDT', 'BCHBUSD', 'EOSBUSD', 'XRPBUSD', 'LTCBUSD', 'ETHBUSD', 'BNBBUSD', 'ETCBUSD', 'TRXBUSD', 'ADABUSD', 'ETHUSDC', 'ZILBTC', 'ZILUSDT', 'TFUELBTC', 'TFUELUSDT', 'FTMBTC', 'FTMUSDT', 'SXPBTC', 'SXPUSDT', 'DOTBTC', 'DOTUSDT', 'ALGOBTC', 'ALGOUSDT', 'THETABTC', 'THETAUSDT', 'COMPBTC', 'COMPUSDT', 'OMGBTC', 'OMGUSDT', 'KAVABTC', 'KAVAUSDT', 'DOGEBTC', 'DOGEUSDT', 'WAVESBTC', 'WAVESUSDT', 'SNXBTC', 'SNXUSDT', 'YFIBTC', 'YFIUSDT', 'CRVBTC', 'CRVUSDT', 'SUSHIBTC', 'SUSHIUSDT', 'UNIBTC', 'UNIUSDT', 'AVAXBTC', 'AVAXUSDT', 'YFIIBTC', 'YFIIUSDT', 'NEARBTC', 'NEARUSDT', 'FILBTC', 'FILUSDT', 'XLMBUSD', 'UNIBUSD', 'NEOBUSD', 'YFIBUSD', 'XMRBUSD', 'DASHBUSD', 'VETBUSD', 'XTZBUSD', 'AAVEBTC', 'AAVEUSDT', 'BTCEUR', 'EURUSDT', 'EURBUSD', 'ETHEUR', 'CHZUSDT', 'CHZBTC', 'TRXETH', 'LINKETH', 'EOSETH', 'ADAETH', 'LTCETH', 'XMRETH', 'YFIEUR', 'BCHEUR', 'GBPBUSD', 'GBPUSDT', 'ETHGBP', 'BTCGBP', 'GRTBTC', 'GRTETH', 'GRTUSDT', 'CRVBUSD', 'ALGOBUSD', 'KSMUSDT', 'KSMBTC', 'ZILBUSD', 'ONTBUSD', 'BATBUSD', 'EGLDUSDT', 'EGLDBTC', 'EGLDBUSD', '1INCHUSDT', '1INCHBTC', 'EOSUSDC', 'LTCUSDC', 'XRPUSDC', 'USDCBUSD', 'CAKEBTC', 'CAKEBUSD', 'CAKEUSDT', 'BTTUSDT', 'BTTBUSD', 'KSMBUSD', 'SOLBTC', 'SOLUSDT', 'SOLBUSD', 'MATICBUSD']

# if 'BTC' + token in pairs:
#     side = 'BUY'
#     symbol = 'BTC' + token
# else:
side = 'SELL'
symbol = token + 'BTC'

print(side)

def request(url, method, params, signature = False):
    headers = {
            "X-MBX-APIKEY": public_key
        }

    signature = hmac.new( private_key.encode(), 'a'.encode(), hashlib.sha256 ).hexdigest()

    if signature:
        params['signature'] = signature
        params['timestamp'] = current_time()

    if method=='GET':
        params = urllib.parse.urlencode(params)
        r = requests.get(f"{url}?{params}", headers=headers)

    if method=='POST':
        r = requests.get(f"{url}", headers=headers, data=params)

    return r.json()

request('https://api.binance.com/sapi/v1/margin/order', 'POST', {
        symbol: symbol,
        side: side,
        type: 'LIMIT',
        price: ,
        sideEffectType: 'MARGIN_BUY'
    }, signature=True)
