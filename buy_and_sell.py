import time
import os
import hmac
import hashlib
import urllib
import requests
import numpy as np
import sys
import sqlite3
from decimal import Decimal
import math

def current_time():
    return round(time.time() * 1000)

def round_str(number, round_to):
    return "{}0:.{}f{}".format("{", round_to, "}").format(number)

public_key='ytGs1cfDtC27uMNiefnfb1p1eFhtPXBtjsQK1fojrBUvhLMuCvMB9F3Ypa8BjLlW'
private_key='KEjSwR9iCdR5jB7inea2t0KMuu2AhGs5qvzhu7HOinT3CWu14y4fJRsCIK2B3Jak'

# if 'BTC' + token in pairs:
#     side = 'BUY'
#     symbol = 'BTC' + token
# else:
token = sys.argv[1]
symbol = token + 'BTC'

def request(url, method, params, signature = False):
    headers = {
            "X-MBX-APIKEY": public_key
        }

    if signature:
        params['timestamp'] = current_time()
        signature = hmac.new(private_key.encode(), urllib.parse.urlencode(params).encode(), hashlib.sha256 ).hexdigest()
        params['signature'] = signature

    params = urllib.parse.urlencode(params)

    if method=='GET':
        r = requests.get(f"{url}?{params}", headers=headers)

    if method=='POST':
        r = requests.post(url, headers=headers, data=params)

    return r.json()

print(symbol)

conn = sqlite3.connect('coins.db')

cursor = conn.execute(f"SELECT * from PAIRS where SYMBOL = \'{symbol}\';")

_, _, round_to, round_price_to, price = cursor.fetchone()

btc_amount = 0.001
price = float(price)
sell_price = round_str(price*1.25, round_price_to)
quantity = btc_amount / price
quantity = round(Decimal(math.floor(Decimal(quantity)*10**round_to)/10**round_to), round_to)

print("BUY request with symbol {} and quantity {}".format(symbol, str(quantity)))
r = request('https://api.binance.com/api/v3/order', 'POST', {
        'symbol': symbol,
        'side': 'BUY',
        'quantity': quantity,
        'type': 'MARKET',
    }, signature=True)
print(r)

code = -1013
while code == -1013:
    coins = request('https://api.binance.com/sapi/v1/capital/config/getall', 'GET', {
        }, signature=True)

    quantity = [coin['free'] for coin in coins if coin['coin'] == token][0]
    quantity = round(Decimal(math.floor(Decimal(quantity)*10**round_to)/10**round_to), round_to)
    print("Quantity in wallet: {}".format(quantity))

    #Limit Sell
    r = request('https://api.binance.com/api/v3/order', 'POST', {
            'symbol': symbol,
            'side': 'SELL',
            'quantity': quantity,
            'type': 'LIMIT',
            'price': sell_price,
            'timeInForce': 'GTC',
        }, signature=True)
    print("SELL request with symbol {}, quantity {} and price {}".format(symbol, quantity, sell_price))
    print(r)

    #Market Sell
    # r = request('https://api.binance.com/api/v3/order', 'POST', {
    #         'symbol': symbol,
    #         'side': 'SELL',
    #         'quantity': quantity,
    #         'type': 'MARKET',
    #     }, signature=True)
    # print(r)

    code = r['code']
