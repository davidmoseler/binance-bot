import time
import os
import hmac
import hashlib
import urllib
import requests
import numpy as np
import sys
import sqlite3

def current_time():
    return round(time.time() * 1000)

public_key='ytGs1cfDtC27uMNiefnfb1p1eFhtPXBtjsQK1fojrBUvhLMuCvMB9F3Ypa8BjLlW'
private_key='KEjSwR9iCdR5jB7inea2t0KMuu2AhGs5qvzhu7HOinT3CWu14y4fJRsCIK2B3Jak'

def request(url, method, params, signature = False):
    headers = {
            "X-MBX-APIKEY": public_key
        }

    if signature:
        params['timestamp'] = current_time()
        signature = hmac.new(private_key.encode(), urllib.parse.urlencode(params).encode(), hashlib.sha256 ).hexdigest()
        params['signature'] = signature

    params = urllib.parse.urlencode(params)

    if method=='GET':
        r = requests.get(f"{url}?{params}", headers=headers)

    if method=='POST':
        r = requests.post(url, headers=headers, data=params)

    return r.json()

symbols = {}

conn = sqlite3.connect('coins.db')

conn.execute('''CREATE TABLE pairs
	(id INTEGER PRIMARY KEY,
	symbol TEXT NOT NULL,
	round_to INTEGER,
	round_price_to INTEGER,
	price TEXT);''');

r = request('https://api.binance.com/api/v3/exchangeInfo', 'GET', {
    }, signature=False)

for symbol in r['symbols']:
    conn.execute(f"INSERT INTO PAIRS (SYMBOL) VALUES (\'{symbol['symbol']}\')")

conn.commit()
